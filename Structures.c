#include <stdio.h>
struct student{
    char Name[60];
    char Subject [20];
    float Marks ;
} s[100];

void main(){
    int a,n;
    printf("Enter the number of Students : \n");
    scanf ("%d", &n);
    printf ("*Please Enter the Student Details*\n");

    //Recording Details
    for (a=0; a<n; a++ ){
        printf ("Enter the Name:\n");
        scanf("%s", s[a].Name);
        printf ("Enter the Subject:\n");
        scanf ("%s", &s[a].Subject);
        printf ("Enter Marks :\n");
        scanf ("%f", &s[a].Marks);
    }
    //Displaying Details
   printf ("*Displaying Details*\n\n");

    for (a=0; a<n; a++ ){
       printf ("Name of the Student:" );
       puts (s[a].Name);
       printf ("Subject:");
       puts (s[a].Subject);
       printf ("Marks :%0.2f\n", s[a].Marks);
       printf("\n");

   }
return 0;


}

